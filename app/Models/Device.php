<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    
    public $timestamps = false;

            /**
     * The attributes that are mass assignable.
     *
     * @var array $fillable
     */
    protected $fillable = [
        'model',
        'platform',
        'uuid',
        'version'
    ];

    
    public function attachedEvents()
    {
        return $this->hasMany(\App\Models\LiveDevice::class);
    }


}