<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Slugger extends MyBaseModel
{
    use SoftDeletes;
}
