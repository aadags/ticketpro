<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class LiveDevice extends MyBaseModel
{
    use SoftDeletes;

    public function event()
    {
        return $this->belongsTo(\App\Models\Event::class);
    }

    public function device()
    {
        return $this->belongsTo(\App\Models\Device::class);
    }

}
