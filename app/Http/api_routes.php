<?php

Route::group(['prefix' => 'api'], function () {

    Route::group(['middleware' => ['cors']], function () {

    /*
     * ---------------
     * Organisers
     * ---------------
     */

    Route::post('/mobile/device/add', [
        'as'   => 'postAddDevice',
        'uses' => 'MobileController@postAddDevice',
    ]);

    Route::get('/mobile/check_in/{app_id}/events', [
        'as'   => 'getLiveEvents',
        'uses' => 'MobileController@getLiveEvents',
    ]);

    // Route::post('/mobile/{event_id}/check_in/search', [
    //     'as'   => 'postCheckInSearch',
    //     'uses' => 'EventCheckInController@postCheckInSearch',
    // ]);
    
    // Route::post('/mobile/{event_id}/check_in/', [
    //     'as'   => 'postCheckInAttendee',
    //     'uses' => 'EventCheckInController@postCheckInAttendee',
    // ]);

    Route::post('/mobile/{event_id}/qrcode_check_in', [
        'as'   => 'postQRCodeCheckInAttendee',
        'uses' => 'MobileController@postCheckInAttendeeQr',
    ]);

    // Route::post('/mobile/{event_id}/confirm_order_tickets/{order_id}', [
    //     'as'   => 'confirmCheckInOrderTickets',
    //     'uses' => 'EventCheckInController@confirmOrderTicketsQr',
    // ]);


    /*
     * ---------------
     * Events
     * ---------------
     */
    Route::resource('events', 'API\EventsApiController');


    /*
     * ---------------
     * Attendees
     * ---------------
     */
    Route::resource('attendees', 'API\AttendeesApiController');


    /*
     * ---------------
     * Orders
     * ---------------
     */

    /*
     * ---------------
     * Users
     * ---------------
     */

    /*
     * ---------------
     * Check-In / Check-Out
     * ---------------
     */


    });
});