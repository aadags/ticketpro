<?php

namespace App\Http\Controllers;

use App\Models\Attendee;
use App\Models\Event;
use App\Models\Slugger;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use JavaScript;

class FrontViewController extends Controller
{
    /**
     * Show the check-in page
     *
     * @param $event_id
     * @return \Illuminate\View\View
     */
    public function showHome()
    {
        $topThree = Event::where('is_live', '=', 1)->where('end_date', '>=', (new Carbon('now'))->hour(0)->minute(0)->second(0))->orderBy('start_date', 'asc')->limit(6)->get();
        // $nextSix = Event::where('is_live', '=', 1)->where('end_date', '>=', (new Carbon('now'))->hour(0)->minute(0)->second(0))->orderBy('start_date', 'asc')->offset(3)->limit(6)->get();
        // $topViewed = DB::select('SELECT event_id, events.title, events.description, events.start_date FROM event_stats inner join events on events.id = event_stats.event_id 
        //                             WHERE events.is_live = 1 AND events.end_date >= NOW() GROUP BY event_id, events.title, events.description, events.start_date ORDER BY MAX(unique_views) DESC LIMIT 4');        

        $data = [
            'topThreeEvents'    => $topThree,
            //'nextSixEvents'    => $nextSix,
            //'topViewedEvents'    => $topViewed,
            'eventModel'    => new Event,
        ];
        
        return view('Public.FrontView.Home', $data);
    }

    public function showEvents(Request $request)
    {
        $searchString = $request->query('search');
        $cat = $request->query('category');

        $events = Event::where('is_live', '=', 1)
                ->where('end_date', '>=', (new Carbon('now'))->hour(0)->minute(0)->second(0));
        
        if(!empty($searchString)){
            $events = $events->where('title', 'LIKE', '%'.$searchString.'%')->orWhere('description', 'LIKE', '%'.$searchString.'%')->orWhere('venue_name_full', 'LIKE', '%'.$searchString.'%');
        }

        if(!empty($cat) && $cat !== 'all'){
            $events = $events->where('event_type', '=', $cat);
        }

        $events = $events->orderBy('start_date', 'asc')->paginate(10);

        $eventsJson = json_encode($events);
        
        $data = [
            'events'    => $events,
            'eventsJson'    => json_decode($eventsJson),
            'searchString'    => $searchString,
            'cat'    => $cat,
        ];
        
        return view('Public.FrontView.Events', $data);
    }

    public function fancyUrl(Request $request, $slug)
    {
        $link = Slugger::where('mask', '=', $slug)->firstOrFail();

        $url = ($link->real[0] == '/')? $link->real : '/'.$link->real;

        return redirect()->to($link->real);
    }

}
