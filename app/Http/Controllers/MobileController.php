<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Attendee;
use App\Models\Device;
use App\Models\Event;
use Validator;
use Carbon\Carbon;
use GuzzleHttp\Client;

class MobileController extends Controller
{
        /**
     * Edit an account
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postAddDevice(Request $request)
    {                 

        $v = Validator::make($request->all(), [
            'model' => 'required|string',
            'platform' => 'required|string',
            'uuid' => 'required|string',
            'version' => 'required|string'
        ]);

        if ($v->fails()) {
            return response()->json([
                "status" => "failed",
                "message" => "Validation failed. all parseable fields must be string",
            ], 422);
        }                       
        
        $device = Device::where('uuid', $request->uuid)->where('model', $request->model)->first();
        if(count($device) < 1){
            $device = new Device;
            $device->uuid = $request->uuid;
            $device->model = $request->model;
            $device->platform = $request->platform;
            $device->version = $request->version;
            $device->save();
            $rand = rand(1, 9999);
            $r = str_pad($rand, 4, "0", STR_PAD_LEFT);
            $l = str_pad($device->id, 3, "0", STR_PAD_LEFT);
            $device->oid = $l.'-'.$r;
            $device->save();
        }
        
        return response()->json([
            "status" => "success",
            "oid" => $device->oid,
        ], 200); 
    }


    public function getLiveEvents($app_id = '')
    { 
        $device = Device::where('oid', $app_id)->firstOrFail();

        $attachedEvents = Event::join('live_devices', 'events.id', '=', 'live_devices.event_id')->where('events.is_live', '=', 1)
                            ->where('events.end_date', '>=', (new Carbon('now'))->hour(0)->minute(0)->second(0))
                            ->select('events.*')->where('live_devices.device_id', $device->id)->get();

        return response()->json([
            "status" => "success",
            "events" => $attachedEvents,
        ], 200);
    }


    public function postCheckInAttendeeQr($event_id, Request $request)
    {
        $event = Event::findOrFail($event_id);

        $v = Validator::make($request->all(), [
            'attendee_reference' => 'required',
        ]);

        if ($v->fails()) {
            return response()->json([
                "status" => "failed",
                "message" => "Validation failed. all parseable fields required",
            ], 422);
        }  

        $qrcodeToken = $request->attendee_reference;
        $attendee = Attendee::join('tickets', 'tickets.id', '=', 'attendees.ticket_id')
            ->where(function ($query) use ($event, $qrcodeToken) {
                $query->where('attendees.event_id', $event->id)
                    ->where('attendees.private_reference_number', $qrcodeToken)
                    ->where('attendees.is_cancelled', '=', 0);
            })->select([
                'attendees.id',
                'attendees.order_id',
                'attendees.first_name',
                'attendees.last_name',
                'attendees.email',
                'attendees.reference_index',
                'attendees.arrival_time',
                'attendees.has_arrived',
                'tickets.title as ticket',
            ])->first();

        if (is_null($attendee)) {
            return response()->json([
                'status'  => 'error',
                'message' => "Invalid Ticket! Please try again."
            ]);
        }

        $relatedAttendesCount = Attendee::where('id', '!=', $attendee->id)
            ->where([
                'order_id'    => $attendee->order_id,
                'has_arrived' => false
            ])->count();

        if ($relatedAttendesCount >= 1) {
            $confirmOrderTicketsRoute = route('confirmCheckInOrderTickets', [$event->id, $attendee->order_id]);

            /*
             * @todo Incorporate this feature into the new design
             */
            //$appendedText = '<br><br><form class="ajax" action="' . $confirmOrderTicketsRoute . '" method="POST">' . csrf_field() . '<button class="btn btn-primary btn-sm" type="submit"><i class="ico-ticket"></i> Check in all tickets associated to this order</button></form>';
        } else {
            $appendedText = '';
        }

        if ($attendee->has_arrived) {
            return response()->json([
                'status'  => 'error',
                'message' => 'Attendee already checked in at ' . $attendee->arrival_time->format('H:i A, F j') . $appendedText
            ]);
        }

        Attendee::find($attendee->id)->update(['has_arrived' => true, 'arrival_time' => Carbon::now()]);

        return response()->json([
            'status'  => 'success',
            'message' => 'Success !<br>Name: ' . $attendee->first_name . ' ' . $attendee->last_name . '<br>Reference: ' . $attendee->reference . '<br>Ticket: ' . $attendee->ticket . '.' . $appendedText
        ]);
    }

}
