<?php

namespace App\Http\Controllers;

use App\Models\Organiser;
use App\Models\User;
use Carbon\Carbon;
use Auth;
use Redirect;

class OrganiserDashboardController extends MyBaseController
{
    /**
     * Show the organiser dashboard
     *
     * @param $organiser_id
     * @return mixed
     */
    public function showDashboard($organiser_id)
    {
        if (Organiser::across()->count() > 1){ 
            $org = Organiser::Where('id', $organiser_id)->first();
            $getUser = User::where('account_id', '=', $org->account_id)
            ->where('email', '=', Auth::user()->email)
            ->first();
            Auth::loginUsingId($getUser->id, true);
        }
        
        $organiser = Organiser::scope()->findOrFail($organiser_id);
        $upcoming_events = $organiser->events()->where('end_date', '>=', Carbon::now())->get();
        $calendar_events = [];

        /* Prepare JSON array for events for use in the dashboard calendar */
        foreach ($organiser->events as $event) {
            $calendar_events[] = [
                'title' => $event->title,
                'start' => $event->start_date->toIso8601String(),
                'end'   => $event->end_date->toIso8601String(),
                'url'   => route('showEventDashboard', [
                    'event_id' => $event->id
                ]),
                'color' => '#000000'
            ];
        }

        $data = [
            'organiser'       => $organiser,
            'upcoming_events' => $upcoming_events,
            'calendar_events' => json_encode($calendar_events),
        ];

        return view('ManageOrganiser.Dashboard', $data);
    }
}
