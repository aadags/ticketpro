<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LiveDevice;
use App\Models\Event;
use App\Models\Device;
use Validator;

class EventDevicesController extends MyBaseController
{
    public function showEventDevices(Request $request, $event_id = '')
    { 
        $event = Event::scope()->find($event_id);
        $attachedDevices = $event->attachedDevices()->get();

        $data = [
            'event'      => $event,
            'attachedDevices'      => $attachedDevices,
        ];

        return view('ManageEvent.EventDevices', $data);
    }

    public function showAddDevices(Request $request, $event_id)
    {
        $event = Event::scope()->find($event_id);

        $data = [
            'event'     => $event,
            'modal_id'  => $request->get('modal_id'),
        ];

        return view('ManageEvent.Modals.AssignDevice', $data);
    }

    public function postEventDevices(Request $request, $event_id = '')
    { 
        $rules = [
            'app_id' => ['required'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status'   => 'error',
                'messages' => $validator->messages()->toArray(),
            ]);
        }

        $event = Event::scope()->findOrFail($event_id);
        $device = Device::where('oid', $request->get('app_id'))->firstOrFail();
        $link = new LiveDevice;
        $link->event_id = $event->id;
        $link->device_id = $device->id;
        $link->save();

        \Session::flash('message', 'The device has been authorized to check in for this event');

        return response()->json([
            'status'      => 'success',
            'redirectUrl' => '',
        ]);
    }
}
