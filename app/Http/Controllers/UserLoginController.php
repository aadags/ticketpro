<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Redirect;
use View;
use Mail;

class UserLoginController extends Controller
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->middleware('guest');
    }

    /**
     * Shows login form.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function showLogin(Request $request)
    {
        /*
         * If there's an ajax request to the login page assume the person has been
         * logged out and redirect them to the login page
         */
        if ($request->ajax()) {
            return response()->json([
                'status'      => 'success',
                'redirectUrl' => route('login'),
            ]);
        }

        return View::make('Public.LoginAndRegister.Login');
    }

    /**
     * Handles the login request.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function postLogin(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');

        if (empty($email) || empty($password)) {
            return Redirect::back()
                ->with(['message' => 'Please fill in your email and password', 'failed' => true])
                ->withInput();
        }

        if ($this->auth->attempt(['email' => $email, 'password' => $password], true) === false) {
            return Redirect::back()
                ->with(['message' => 'Your username/password combination was incorrect', 'failed' => true])
                ->withInput();
        }

        if(Auth::user()->account->is_banned > 0){
            $this->auth->logout();
            return Redirect::back()
            ->with(['message' => 'Your account has been temporarily suspended', 'failed' => true])
            ->withInput();    
        }

      
        
        if(Auth::user()->is_confirmed < 1){
            
            $user = Auth::user();
            $this->auth->logout();

            Mail::send('Emails.ConfirmEmail',
            ['first_name' => $user->first_name, 'confirmation_code' => $user->confirmation_code],
            function ($message) use ($user) {
                $message->to($user->email, $user->first_name)
                    ->subject('Welcome to OgaTicketr');
            });

            return Redirect::back()
            ->with(['message' => 'You are yet to verify your email. A new verification email has been sent to you.', 'failed' => true])
            ->withInput();    
        }



        return redirect()->intended(route('showSelectOrganiser'));
    }
}
