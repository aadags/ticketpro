<?php

use Illuminate\Database\Seeder;

class PaymentGatewaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payment_gateways = [
            [
                'id' => 1,
                'name' => 'Stripe',
                'provider_name' => 'Stripe',
                'provider_url' => 'https://www.stripe.com',
                'is_on_site' => 1,
                'can_refund' => 1,
            ],
            [
                'id' => 2,
                'name' => 'Paystack',
                'provider_name' => 'Paystack',
                'provider_url' => 'https://www.paystack.com',
                'is_on_site' => 1,
                'can_refund' => 0

            ],
        ];

        DB::table('payment_gateways')->insert($payment_gateways);

    }
}
