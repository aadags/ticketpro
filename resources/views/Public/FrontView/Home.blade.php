@extends('Public.FrontView.Layouts.Main')

@section('title')
    Welcome
@stop

@section('headerclass')
header-section sticky-header-section not-stuck clearfix
@stop

@section('content')

<!-- slide-section - start
    ================================================== -->
    <section id="slide-section" class="slide-section clearfix">
        <div id="main-carousel1" class="main-carousel1 owl-carousel owl-theme">

            <div class="item" style="background-image: url(assets/images/slider/tiwa.jpg);">
                <div class="overlay-black">
                    <div class="container">
                        <div class="slider-item-content">

                            <span class="medium-text">one stop</span>
                            <h1 class="big-text">Event Ticketer</h1>
                            <small class="small-text">every event should be perfect</small>

                            <div class="link-groups">
                                <a href="{{ route('login') }}" class="about-btn custom-btn">Create Event</a>
                                <a href="{{ route('events') }}" class="start-btn">Browse Events</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="item" style="background-image: url(assets/images/slider/wizzy.jpg);">
                <div class="overlay-black">
                    <div class="container">
                        <div class="slider-item-content">

                            <h1 class="big-text">Paid or Free Event?</h1>
                            <span class="medium-text">Manage your attendees and tickets easily</span>

                            <div class="link-groups">
                                <a href="{{ route('login') }}" class="about-btn custom-btn">Create Event</a>
                                <a href="{{ route('events') }}" class="start-btn">Browse Events</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!--div class="item" style="background-image: url(assets/images/slider/davido.jpg);">
                <div class="overlay-black">
                    <div class="container">
                        <div class="slider-item-content">

                            <span class="medium-text">one stop</span>
                            <h1 class="big-text">Event Planner</h1>
                            <small class="small-text">every event sould be perfect</small>

                            <div class="link-groups">
                                <a href="about.html" class="about-btn custom-btn">about us</a>
                                <a href="#!" class="start-btn">get started!</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div-->

        </div>
    </section>
    <!-- slide-section - end
    ================================================== -->





    <!-- upcomming-event-section - start
    ================================================== -->
    <section id="upcomming-event-section" class="upcomming-event-section sec-ptb-100 clearfix">
        <div class="container">

            <!-- section-title - start -->
            <div class="section-title text-center mb-50">
                <small class="sub-title">upcoming events</small>
                <h2 class="big-title"> @if(count($topThreeEvents) < 1) No upcoming events at this time. @endif </h2>
            </div>
            <!-- section-title - end -->

            <!-- upcomming-event-carousel - start -->
            <div id="upcomming-event-carousel" class="upcomming-event-carousel owl-carousel owl-theme">
                @foreach($topThreeEvents as $topThreeEvent)
                <!-- item - start -->
                <div class="item">
                    <div class="event-item">

                        <div class="countdown-timer">
                            <ul class="countdown-list" data-countdown="{{ $topThreeEvent->start_date->format('Y/m/d h:i a') }}"></ul>
                        </div>

                        <div class="event-image">
                            <img src="{{ $topThreeEvent->images->first()['image_path'] }}" alt="Image_not_found">
                            <div class="post-date">
                                <span class="date">{{ $topThreeEvent->start_date->format('d') }}</span>
                                <small class="month">{{ $topThreeEvent->start_date->format('M'."'".' y') }}</small>
                            </div>
                        </div>

                        <div class="event-content">
                            <div class="event-title mb-30">
                                <h3 class="title">
                                    {{ $topThreeEvent->title }}
                                </h3>
                            </div>
                            <div class="event-post-meta ul-li-block mb-30">
                                <ul>
                                    <li>
                                        <span class="icon">
                                            <i class="far fa-clock"></i>
                                        </span>
                                        {{ $topThreeEvent->start_date->format('h:i a') }} - {{ $topThreeEvent->end_date->format('h:i a') }}
                                    </li>
                                    <li>
                                        <span class="icon">
                                            <i class="fas fa-map-marker-alt"></i>
                                        </span>
                                        {{ $topThreeEvent->venue_name_full }}
                                    </li>
                                </ul>
                            </div>
                            <a href="{{ $topThreeEvent->getEventUrlAttribute() }}" class="custom-btn">
                                tickets & details
                            </a>
                        </div>

                    </div>
                </div>
                <!-- item - end -->
                @endforeach

            </div>
            <!-- upcomming-event-carousel - end -->

        </div>
    </section>
    <!-- upcomming-event-section - end
    ================================================== -->





    <!-- about-section - start
    ================================================== -->
    <section id="about-section" class="about-section sec-ptb-100 clearfix">
        <div class="container">
            <div class="row">

                <!-- section-title - start -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="section-title text-left mb-30">
                        <span class="line-style"></span>
                        <small class="sub-title">we are oga at ticketing</small>
                        <h2 class="big-title">For all your concerts, parties, festivals, meetups, seminars etc...</h2>
                        
                        <a href="{{ route('aboutUs') }}" class="custom-btn">
                            more about ogaticketr
                        </a>
                    </div>
                </div>
                <!-- section-title - end -->

            </div>
        </div>
    </section>
    <!-- about-section - end
    ================================================== -->





    <!-- conference-section - start
    ================================================== -->
    <section id="conference-section" class="conference-section clearfix">
        <div class="jarallax" style="background-image: url(assets/images/conference/pexels-photo-262669.jpg);">
            <div class="overlay-black sec-ptb-100">

                <div class="mb-50">
                    <div class="container">
                        <div class="row">

                            <!-- section-title - start -->
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="section-title text-left">
                                    <span class="line-style"></span>
                                    <small class="sub-title">Videos</small>
                                    <h2 class="big-title">Awesome concerts & music from around the world.</strong></h2>
                                </div>
                            </div>
                            <!-- section-title - end -->

                        </div>
                    </div>
                </div>

                <!-- conference-content-wrapper - start -->
                <div class="tab-wrapper">

                    <!-- tab-menu - start -->
                    <div class="container">
                        <div class="row justify-content-lg-start">
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="tab-menu">
                                    <ul class="nav tab-nav mb-50">

                                        <li class="nav-item">
                                            <a class="nav-link active" id="nav-one-tab" data-toggle="tab" href="#nav-one" aria-expanded="true">
                                                <span class="image">
                                                    <img src="assets/images/conference/AMVCA-2018-Flow.jpg" alt="Image_not_found">
                                                </span>
                                                <span class="title">
                                                    AMVCA AWARDS 2018
                                                </span>
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" id="nav-two-tab" data-toggle="tab" href="#nav-two" aria-expanded="true">
                                                <span class="image">
                                                    <img src="assets/images/conference/oneafrica.jpg" alt="Image_not_found">
                                                </span>
                                                <span class="title">
                                                    One Africa Music Fest London 2018 - Highlight
                                                </span>
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" id="nav-three-tab" data-toggle="tab" href="#nav-three" aria-expanded="true">
                                                <span class="image">
                                                    <img src="assets/images/conference/mvp.jpg" alt="Image_not_found">
                                                </span>
                                                <span class="title">
                                                    BEST OF BASKETMOUTH AT SOUNDCITY MVP AWARDS FESTIVAL 2016
                                                </span>
                                            </a>
                                        </li>

                                    </ul>
                                    <div class="more-btn">
                                        <a href="#!">
                                            <strong class="yellow-color">Youtube</strong> channel
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- tab-menu - end -->

                    <!-- tab-content - start -->
                    <div class="tab-content">
                        <!-- tab-pane - start -->
                        <div class="tab-pane fade active show" id="nav-one" role="tabpanel" aria-labelledby="nav-one-tab" aria-expanded="true">
                            <div class="image">
                                <iframe width="99%" height="99%" src="https://www.youtube.com/embed/xTsiUj4QIzA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>   
                            </div>
                        </div>
                        <!-- tab-pane - end -->

                        <!-- tab-pane - start -->
                        <div class="tab-pane fade" id="nav-two" role="tabpanel" aria-labelledby="nav-two-tab" aria-expanded="true">
                            <div class="image">
                                <iframe width="99%" height="99%" src="https://www.youtube.com/embed/y8hz2D5VIAM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                        <!-- tab-pane - end -->

                        <!-- tab-pane - start -->
                        <div class="tab-pane fade" id="nav-three" role="tabpanel" aria-labelledby="nav-three-tab" aria-expanded="true">
                            <div class="image">
                                    <iframe width="99%" height="99%" src="https://www.youtube.com/embed/XK_EO2RTsbY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                        <!-- tab-pane - end -->

                    </div>
                    <!-- tab-content - end -->

                </div>
                <!-- conference-content-wrapper - end -->

            </div>
        </div>
    </section>
    <!-- conference-section - end
    ================================================== -->

    
@stop