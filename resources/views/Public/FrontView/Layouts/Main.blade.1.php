
<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>@yield('title')- OgaTicketr - Manage Events and Sell Tickets Online</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="description" content="Default Description" />
<meta name="keywords" content="event management", "ticket selling, tickets, events" />
<meta name="robots" content="INDEX,FOLLOW" />
@include('Shared.Partials.GlobalMeta')
<link href='//fonts.googleapis.com/css?family=Roboto+Slab:400,300,700&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.min.css" />


<link rel="stylesheet" type="text/css" href="{{ asset("magento_52944/skin/frontend/default/theme686/css/jquery.bxslider.css") }}" media="all" />
<link rel="stylesheet" type="text/css" href="{{ asset("magento_52944/skin/frontend/default/theme686/css/photoswipe.css") }}" media="all" />
<link rel="stylesheet" type="text/css" href="{{ asset("magento_52944/skin/frontend/default/theme686/css/bootstrap.css") }}" media="all" />
<link rel="stylesheet" type="text/css" href="{{ asset("magento_52944/skin/frontend/default/theme686/css/extra_style.css") }}" media="all" />
<link rel="stylesheet" type="text/css" href="{{ asset("magento_52944/skin/frontend/default/theme686/css/styles.css") }}" media="all" />
<link rel="stylesheet" type="text/css" href="{{ asset("magento_52944/skin/frontend/default/theme686/css/responsive.css") }}" media="all" />
<link rel="stylesheet" type="text/css" href="{{ asset("magento_52944/skin/frontend/default/theme686/css/superfish.css") }}" media="all" />
<link rel="stylesheet" type="text/css" href="{{ asset("magento_52944/skin/frontend/default/theme686/css/camera.css") }}" media="all" />
<link rel="stylesheet" type="text/css" href="{{ asset("magento_52944/skin/frontend/base/default/css/widgets.css") }}" media="all" />
<link rel="stylesheet" type="text/css" href="{{ asset("magento_52944/skin/frontend/default/theme686/aw_blog/css/style.css") }}" media="all" />
<link rel="stylesheet" type="text/css" href="{{ asset("magento_52944/skin/frontend/default/theme686/css/cmsmart/megamenu/megamenu.css") }}" media="all" />
<link rel="stylesheet" type="text/css" href="{{ asset("magento_52944/skin/frontend/default/theme686/css/ecommerceteam/cloud-zoom.css") }}" media="all" />
<link rel="stylesheet" type="text/css" href="{{ asset("magento_52944/skin/frontend/default/theme686/css/catalogsale.css") }}" media="all" />

<script type="text/javascript" src="{{ asset("magento_52944/js/jquery/jquery-1.11.1.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/js/jquery/jquery-migrate-1.2.1.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/js/jquery/jquery_noconflict.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/js/prototype/prototype.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/js/lib/ccard.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/js/prototype/validation.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/js/scriptaculous/builder.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/js/scriptaculous/effects.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/js/scriptaculous/dragdrop.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/js/scriptaculous/controls.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/js/scriptaculous/slider.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/js/varien/js.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/js/varien/form.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/js/mage/translate.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/js/mage/cookies.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/js/ecommerceteam/cloud-zoom.1.0.2.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/skin/frontend/default/theme686/js/bootstrap.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/skin/frontend/default/theme686/js/jquery.easing.1.3.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/skin/frontend/default/theme686/js/jquery.mobile.customized.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/skin/frontend/default/theme686/js/jquery.carouFredSel-6.2.1.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/skin/frontend/default/theme686/js/jquery.touchSwipe.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/skin/frontend/default/theme686/js/jquery.bxslider.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/skin/frontend/default/theme686/js/jquery.unveil.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/skin/frontend/default/theme686/js/smoothing-scroll.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/skin/frontend/default/theme686/js/cherry-fixed-parallax.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/skin/frontend/default/theme686/js/tm-stick-up.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/skin/frontend/default/theme686/js/superfish.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/skin/frontend/default/theme686/js/scripts.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/skin/frontend/default/theme686/js/jquery-ui.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/skin/frontend/default/theme686/js/cmsmart/megamenu/cmsmartmenu.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/skin/frontend/default/theme686/js/carousel.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/skin/frontend/default/theme686/js/msrp.js") }}"></script>
<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="{{ asset("magento_52944/skin/frontend/default/theme686/css/styles-ie.css") }}" media="all" />
<![endif]-->
<!--[if lt IE 7]>
<script type="text/javascript" src="{{ asset("magento_52944/js/lib/ds-sleight.js") }}"></script>
<script type="text/javascript" src="{{ asset("magento_52944/skin/frontend/base/default/js/ie6.js") }}"></script>
<![endif]-->

<script type="text/javascript">
//<![CDATA[
Mage.Cookies.path     = '/';
Mage.Cookies.domain   = '.ogaticketr.com';
//]]>
</script>
<script type="text/javascript">//<![CDATA[
        var Translator = new Translate([]);
        //]]></script>

<style>
.header a.logo {
    padding: 0px 0px 0px 0px !important;
}
</style>

</head>
<body class="ps-static  cms-index-index cms-home">
<div class="wrapper ps-static en-lang-class">
        <noscript>
        <div class="global-site-notice noscript">
            <div class="notice-inner">
                <p>
                    <strong>JavaScript seems to be disabled in your browser.</strong><br />
                    You must have JavaScript enabled in your browser to utilize the functionality of this website.                </p>
            </div>
        </div>
    </noscript>
    <div class="page">
       
                           
<div class="header-container">
    <div class="header">
        <div class="quick-access">
             <ul class="links">
                        <li class="first" ><a href="{!! route('howItWorks') !!}" title="How it Works" >How it works</a></li>
                                                     <li class=" last" ><a href="{!! route('aboutUs') !!}" title="About Us" >About Us</a></li>
            </ul>
        </div>
       
        <div class="header-buttons"></div>

        <p class="welcome-msg"><b><a href="/login" title="Organiser Login" >Organiser Login</a></b>
        <a type="button" href="/signup" style="background: #000000; padding: 10px; font-size: 16px; line-height: 18px; color: #fff; font-weight: 700; text-transform: uppercase; height: auto; position: relative; z-index: 0;"><span><span>Manage An Event</span></span></a>
        </p>
      
        <div id="header-account" class="skip-content"></div>
        <div class="clear"></div>
         <h1 class="logo"><a href="/" title="OgaTicketr" class="logo"><img src="{{ asset("assets/images/ogt_logo.png") }}" alt="OgaTicketr" /></a></h1>

            <div class="clear"></div>
            </div>
	<div class="clear"></div>
</div>
            <div class="nav-container-mobile">
    	<div class="container">
    		<div class="row">
                <div class="col-xs-12">
                    <div class="sf-menu-block">
                        <div id="menu-icon">Menu <i class="fa fa-bars"></i></div>
                        <ul class="sf-menu-phone">
                            <li  class="level0 nav-1 first level-top"><a href="/events"  class="level-top" ><span>Browse Events</span></a></li>
                            <!-- <li  class="level0 nav-2 level-top"><a href=""  class="level-top" ><span>Sport Tickets</span></a></li> -->
                            <li  class="level0 nav-3 level-top"><a href="{!! route('howItWorks') !!}"  class="level-top" ><span>How It Works</span></a></li>
                            <li  class="level0 nav-4 level-top"><a href="/signup"  class="level-top" ><span>Manage An Event</span></a></li>
                            <li  class="level0 nav-5 level-top"><a href="/login"  class="level-top" ><span>Organiser Login</span></a></li>
                        </ul>
                        </div>
                </div>
            </div>
    		<div class="clear"></div>
    	</div>
    </div>
    

<div class="nav-container">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
	            <!-- <div class="nav" style="width: ;"> -->
                <div class="nav">
                    <ul id="nav" class="grid-full"> 
                                                                            
                            <li  class="level nav-1 first  no-level-thumbnail ">
<a href="{!! route('events') !!}">
<span>Browse Events</span>
</a>
</li>

<!-- <li  class="level nav-2  no-level-thumbnail ">
<a href="/events/sports">
<span>Sport Tickets</span>
</a>
</li>      -->

<li  class="level nav-3  no-level-thumbnail ">
<a href="{!! route('howItWorks') !!}">
<span>How It Works</span>
</a>
</li>               

<!-- <li  class="level nav-4  no-level-thumbnail ">
<a href="/how">
<span>Create An Event</span>
</a>
</li>                    -->
                     </ul>
                </div>
            </div>
        </div>
	</div>
</div> <!-- end: nav-container -->

@yield('content')

<div class="footer-container">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="footer">
					  <p id="back-top"><a href="#top"><i class="fa fa-caret-up fa-4x"></i></a> </p>
					  <div class="footer-cols-wrapper">
						<div class="footer-col">			
							<h4>Information</h4>
							<div class="footer-col-content">
								<ul>
<li><a href="{!! route('aboutUs') !!}">About Us</a></li>
<li><a href="{!! route('howItWorks') !!}">How it works</a></li>
<li><a href="{!! route('termsAndConditions') !!}">Terms & Conditions</a></li>
        <li class="last privacy"><a href="{!! route('disclaimer') !!}">Disclaimer</a></li>
</ul>							
							</div>
						</div>
						<div class="footer-col">
							<h4>Connect with us</h4>
							<div class="footer-col-content">
								<div class="block block-subscribe">
    
                                </div>
							
							</div>
							<div class="clear"></div>
<div class="social-buttons-block">
	<ul>
		<li><a href="https://twitter.com/ogaticketr" target="_blank" class="fa fa-twitter"></i></a></li>
		<li><a href="https://fb.me/ogaticketr" target="_blank" class="fa fa-facebook"></i></a></li>
		<li><a href="https://www.instagram.com/ogaticketr/" target="_blank" class="fa fa-instagram"></i></a></li>
		<li><a href="mailto:support@ogaticketr.com" target="_blank" class="fa fa-envelope"></i></a></li>
	</ul>
</div>						    
                        </div>
                        
                        <div class="footer-col">
							<h4>Payment Partners</h4>
							
    <img src="{{ asset("assets/images/paystack.png") }}" alt="Paystack" width="50%" style="margin-bottom:3%;"/><br/>
    <img src="{{ asset("assets/images/stripe.png") }}" alt="Stripe" width="35%" />
	
                        </div>
                        <div class="footer-col last">
							<h4>Powered By</h4>
							
	<img src="{{ asset("assets/images/xetabytes.png") }}" alt="Xetabytes" width="50%" style="margin-bottom:3%;" /><br/>
	<img src="{{ asset("assets/images/attendize.png") }}" alt="Attendize" width="50%" />

                        </div>
                        
												<div class="clear"></div>
					  </div>
					<div class="clear"></div>
					<address>&copy; 2017 - <script type="text/javascript">var mdate = new Date(); document.write(mdate.getFullYear());</script> OgaTicketr Event Management & Ticketing Service All Rights Reserved.</address>
					<div class="paypal-logo">
                    </div>
							<!--{%FOOTER_LINK}-->				
                    </div>
			</div>
		</div>
	</div>
</div>
                

    </div>
</div>
</body>
</html>