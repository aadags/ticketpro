@extends('Public.FrontView.Layouts.Main')

@section('title')
    Upcoming Events
@stop

@section('content')

        <div class="main-container col2-left-layout">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="main">
							<div class="breadcrumbs">
    <ul>
                    <li class="home">
                            <a href="/" title="Go to Home Page">Home</a>
                                        <span>></span>
                        </li>
                    <li class="category3">
                            <strong>Upcoming Events</strong>
                                    </li>
            </ul>
</div>
							<div class="row">
								<div class="col-main col-xs-12">
									<div class="padding-s">



<div class="page-title category-title">
        <h1>Upcoming Events</h1>
</div>




    
    <div class="category-products">
    
        
    
                            <ul class="products-grid row">
                            @if(count($events) < 1)
                            <h1>No upcoming events at this time.</h1>
                            @else

                            @foreach($events as $index => $event)

                            @if(in_array($index, array(0,4,8,12,16)))
                            <div class="col-xs-12">
                            @endif
                    <li class="item first col-xs-12 col-sm-3" itemscope itemtype="">
                <div class="item-wrapper">
                    <a href="{{ $event->getEventUrlAttribute() }}" title="" class="product-image" itemprop="url">
                    <img data-src="{{ $event->images->first()['image_path'] }}"  width="270" height="270" class="lazy" alt="" src="{{ $event->images->first()['image_path'] }}" /></a>
                    <div class="product-shop">
                        
    <div class="price-box map-info">
            <span class="old-price" id="product-price-1"><span>{{ $event->start_date->format('d/m/y') }}</span></span>
       
    </div>
                        <h2 class="product-name"><a href="{{ $event->getEventUrlAttribute() }}" itemprop="name">{{ $event->title }}</a></h2>
                                           
                    </div>
                                        <div class="label-product">             
                                                                    </div>                    
                </div>
            </li>
            @if(in_array($index, array(3,7,11,15,19)))
                            </div>                            
            @endif
            @endforeach
            @endif
             
                </ul>
                
                        <script type="text/javascript">decorateGeneric($$('ul.products-grid'), ['odd','even','first','last'])</script>
    
    <div class="toolbar-bottom">
        <div class="toolbar">
    <div class="pager">
    {{ $events->links() }}    

    </div>

        <div class="sorter">
               
    </div>
    </div>
    </div>
</div>


									</div>
								</div>


						</div>
					</div>
				</div>
            </div>
        </div>
   
    
@stop