@extends('Public.FrontView.Layouts.Main')

@section('title')
    How It Works
@stop

@section('headerclass')
header-section default-header-section auto-hide-header clearfix
@stop

@section('bodyclass')
default-header-p
@stop

@section('content')



<!-- breadcrumb-section - start
	================================================== -->
	<section id="breadcrumb-section" class="breadcrumb-section clearfix">
		<div class="jarallax" style="background-image: url(assets/images/breadcrumb/breadconcert.jpg);">
			<div class="overlay-black">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-6 col-md-12 col-sm-12">

							<!-- breadcrumb-title - start -->
							<div class="breadcrumb-title text-center mb-50">
                                    <p class="big-title">all you <strong>need to know</strong></p>
							</div>
							<!-- breadcrumb-title - end -->

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- breadcrumb-section - end
	================================================== -->





	<!-- our-management-section - start
	================================================== -->
	<section id="our-management-section" class="our-management-section bg-gray-light sec-ptb-100 clearfix">
		<div class="container">
			<div class="row">

				<!-- section-title - start -->
				<div class="col-lg-12 col-md-12 col-sm-12">
                        <p>
                                <h1><b>How To Purchase a Ticket on Ogaticketr.</b></h1><br/><br/>
                                <p>1. Explore the website and browse to the Event of your choice.</p>
                                <p>2. On the Event page you will find all the details about the event including the available tickets.</p>
                                <p>3. Choose the number of tickets of your choice you wish to purchase and click on register</p>
                                <p>4. You will be given a limited time to complete your purchase, 
                                    during this time your ticket will be booked temporarily in any case their is a high deman for the event tickets.</p>
                                <p>5. Fill out your details and checkout</p>
                                <p>6. For paid tickets you will be prompted to fill in your card details after which your ticket will be emailed to you. Ensure to use a valid email for purchase.</p>
                                <p>7. Ensure to go to the venue with your e-ticket for checking you into your event.</p>
                                <p>8. If you need fufrther assistance, email us at support@ogaticketr.com or give us a call at (+234) 816 437 5276</p>
                                <br/>   <br/>   
                        
                            
                        </p>

                        <p>
                                <h1><b>For Event Organisers.</b></h1><br/><br/>
                                <p>1. Ogaticketr is a free Event Management and Ticketing service.</p>
                                <p>2. Ogaticketr charges a 10% commission on paid tickets(only), this includes booking fees payment processing fees. Free Tickets are not billed.</p>
                                <p>3. You can choose to pass this fee to your customers if you wish.</p>
                                <p>4. Ogaticketr gives you a wide variety of options which you can use to effectively manage your event:</p>
                                <p>
                                <ul>
                                <li> Fully brandable & customizable - Promote your brand on your event pages</li>
                                <li> Customizable tickets - with QR codes, organiser logos etc.</li>
                                <li> Beautiful mobile friendly event pages</li>
                                <li> Easy attendee management - Refunds, Messaging etc.</li>
                                <li> Data export - attendees list to XLS, CSV etc.</li>
                                <li> Generate print friendly attendee list</li>
                                <li> Ability to manage unlimited events</li>
                                <li> Real-time event statistics</li>
                                <li> Customizable event pages</li>
                                <li> Multiple currency support</li>
                                <li> Quick and easy checkout process</li>
                                <li> Affiliate tracking - track sales volume / number of visits generated etc.</li>
                                <li> Widget support - embed ticket selling widget into existing websites / WordPress blogs</li>
                                <li> Social sharing</li>
                                <li> Messaging - eg. Email all attendees with X ticket</li>
                                <li> Public event listings page for organisers</li>
                                <li> Ability to ask custom questions during checkout</li>
                                <li> Browser based QR code scanner for door management</li>
                                </ul>
                                </p>
                                <p>5. To enjoy these features, simply signup and confirm your valid email address to proceed as an organiser.</p>
                                <p>6. First Timers will be prompted to create an organiser account that will manage your subsequent events</p>
                                <p>7. Link your bank account to your organiser account at the top left corner side menu under "Account Settings" for auto remittance of paid tickets revenue.</p>
                                <p>8. Create your event start sigining on attendees right away.</p>
                                <p>8. Remittance of funds is done automatically within 24 hours fter your event.</p>
                                <p>9. Should in any case you experience any difficulties, email us at support@ogaticketr.com or give us a call at (+234) 816 437 5276.</p>
                                <br/>   <br/>   
                        
                            
                        </p>
				</div>
				<!-- section-title - end -->
				
			</div>
		</div>
	</section>
	<!-- our-management-section - end
	================================================== -->





	<!-- special-offer-section - start
	================================================== -->
	<section id="special-offer-section" class="special-offer-section clearfix" style="background-image: url(assets/images/special-offer-bg.png);">
		<div class="container">
			<div class="row">

				<!-- special-offer-content - start -->
				<div class="col-lg-9">
					<div class="special-offer-content">
						<p>Looking for something <span>special for your event?</span></p>
						<p class="m-0">
							Sign up now and we will make your event ticketing unique & unforgettable
						</p>
					</div>
				</div>
				<!-- special-offer-content - end -->

				<!-- event-makeing-btn - start -->
				<div class="col-lg-3">
					<div class="event-makeing-btn">
						<a href="{{ route('showSignup') }}">sign up now</a>
					</div>
				</div>
				<!-- event-makeing-btn - end -->

			</div>
		</div>
	</section>
	<!-- special-offer-section - end
	================================================== -->





    
@stop