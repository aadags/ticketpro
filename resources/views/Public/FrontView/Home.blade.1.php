@extends('Public.FrontView.Layouts.Main')

@section('title')
    Welcome
@stop

@section('content')
    
<div class="home-top-block">
	<div class="showcase1">
		<ul>
        @foreach($topThreeEvents as $topThreeEvent)
			<li>
				<a href="{{ $topThreeEvent->getEventUrlAttribute() }}">
					<img src="{{ $topThreeEvent->images->first()['image_path'] }}" alt="showcase1_img">
					<span class="showcase1_date">{{ $topThreeEvent->start_date->format('d/m/y') }}</span>
					<div class="showcase1_info">
						<h4>{{ $topThreeEvent->title }}</h4>
						<span></span>
					</div>
				</a>
			</li>
        @endforeach
		</ul>
	</div>
	<div class="showcase2">
		<ul>
        @foreach($nextSixEvents as $nextSixEvent)
			<li>
				<a href="{{ $nextSixEvent->getEventUrlAttribute() }}">
					<img src="{{ $nextSixEvent->images->first()['image_path'] }}" alt="showcase2_img">
					<span class="showcase2_date">{{ $nextSixEvent->start_date->format('d/m/y') }}</span>
					<div class="showcase2_info">
						<h4>{{ $nextSixEvent->title }}</h4>
						<span></span>
					</div>
				</a>
			</li>
        @endforeach
		</ul>
	</div>
</div>	

	<div class="main-container col1-layout">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="main">
															<div class="col-main">
									<div class="padding-s">
									<div class="blog-bg" data-source-url="/assets/images/public/EventPage/backgrounds/30.jpg">
        <div class="widget-latest">
            <div class="container">
                <div class="widget-latest-main-title center">
                    <h2>Free Event Manager and Ticketing Service</h2>
                </div>
                <h2 style="color:white !important;">Take control of your events while minimizing expenditure with fully customizable event manager, ticket designer, attendee management and free scanner for attendee check in & ticket verification.
                </h2>
                <br/>
                <div class="widget-latest-main-title center">
                    <h2>Buy your tickets!</h2>
                </div>
                <h2 style="color:white !important;">
                We sell e-tickets! Your best choice on the web to get your tickets with no stress. e-Ticketing has never been easier.
                </h2>
            </div>
        </div>
    </div>

<div class="page-title category-title">
@if(count($topViewedEvents) > 0)
        <h1>Top Viewed</h1>
@endif
</div>
                    <ul class="products-grid row">
                    @foreach($topViewedEvents as $topViewedEvent)
                    <li class="item col-xs-3 first" itemscope itemtype="">
                        <div class="item-wrapper">
                        <a href="{{ $eventModel->getModel($topViewedEvent->event_id)->getEventUrlAttribute() }}" title="" class="product-image" itemprop="url">
                        <img data-src="{{ $eventModel->getModel($topViewedEvent->event_id)->images->first()['image_path'] }}"  width="270" height="270" class="lazy" alt="" src="{{ $eventModel->getModel($topViewedEvent->event_id)->images->first()['image_path'] }}" />
                        </a>
                        <div class="product-shop">

                        <div class="price-box" itemprop="offers" itemscope itemtype="">
                            <span class="regular-price" itemprop="price" id="product-price-4-new">
                                <span class="price">{{ date('d/m/y', strtotime($topViewedEvent->start_date)) }}</span>                                    
                            </span>
                        </div>

                        <h3 class="product-name">
                            <a href="{{ $eventModel->getModel($topViewedEvent->event_id)->getEventUrlAttribute() }}" title="{{ $topViewedEvent->title }}" itemprop="name">{{ $topViewedEvent->title }}</a>
                        </h3>
                
                        </div>
                                        <div class="label-product">             
                                                                    </div>                    
                        </div>
                    </li>
                    @endforeach
                    </ul>
                    
            <div class="additional-info-1" data-source-url="/assets/images/public/EventPage/backgrounds/13.jpg">
	<div class="container" style="text-align: left !important;">
		<a href="/signup"><h4>Take control of Your Event</h4></a>
		<span><b><ul>
        <li>Fully brandable & customizable - Promote your brand on your event pages</li>
        <li>Customizable tickets - with QR codes, organiser logos etc.</li>
        <li>Beautiful mobile friendly event pages</li>
        <li>Easy attendee management - Refunds, Messaging etc.</li>
        <li>Data export - attendees list to XLS, CSV etc.</li>
        <li>Generate print friendly attendee list</li>
        <li>Ability to manage unlimited events</li>
        <li>Real-time event statistics</li>
        <li>Customizable event pages</li>
        <li>Multiple currency support</li>
        <li>Quick and easy checkout process</li>
        <li>Affiliate tracking - track sales volume / number of visits generated etc.</li>
        <li>Widget support - embed ticket selling widget into existing websites / WordPress blogs</li>
        <li>Social sharing</li>
        <li>Messaging - eg. Email all attendees with X ticket</li>
        <li>Public event listings page for organisers</li>
        <li>Ability to ask custom questions during checkout</li>
        <li>Browser based QR code scanner for door management</li>
        </ul></b></span>	
	</div>
</div>


<div class="widget widget-static-block"><div class="additional-info-2">
	<a href="">
		<h4>OGATICKETR</h4>
		<span>Your biggest choice on the web for event management!</span>
		<div class="clear"></div>			
	</a>
</div></div>

                                    </div>
								</div>
						</div>
					</div>
				</div>
            </div>
        </div>
        
    
@stop