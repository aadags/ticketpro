@extends('Public.FrontView.Layouts.Main')

@section('title')
    About Us
@stop

@section('content')

        <div class="main-container col2-left-layout">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="main">

							<div class="row">
								<div class="col-main col-xs-12">
									<div class="padding-s">



<div class="page-title category-title">
        <h1>About Us</h1>
</div>




    
    <div class="category-products">
	<img src="/assets/images/ogt_logo.png" alt="">
	<br/>
<br/>
<br/>
    <h2>OgaTicketr is an event technology platform. OgaTicketr allows anyone to create, share, find and attend new things to do that fuel their passions and enrich their lives. 
		Music festivals, venues, marathons, conferences, hackathons, air guitar contests, political rallies, fundraisers, gaming competitions — you name it, we power it. 
</h2>
<br/>
<br/>
<br/>
<h2>Our mission? To revolutionalize the events industry through superb and easy free management software.</h2>
<br/>
<br/>
<br/>
<h2><b>Contact Us</b></h2>   
<h2>You can contact us directly by visiting us or give us a phone call:</h2>   
<h2>Address: 73, Arobadade Close, Bariga, Lagos, Nigeria</h2>   
<h2>Phone: +234 816 437 5276</h2>   
<br/>
<br/>
<br/>

    </div>


									</div>
								</div>


						</div>
					</div>
				</div>
            </div>
        </div>
   
    
@stop