@extends('Public.FrontView.Layouts.Main')

@section('title')
    About Us
@stop

@section('headerclass')
header-section default-header-section auto-hide-header clearfix
@stop

@section('bodyclass')
default-header-p
@stop

@section('content')



<!-- breadcrumb-section - start
	================================================== -->
	<section id="breadcrumb-section" class="breadcrumb-section clearfix">
		<div class="jarallax" style="background-image: url(assets/images/breadcrumb/breadconcert.jpg);">
			<div class="overlay-black">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-6 col-md-12 col-sm-12">

							<!-- breadcrumb-title - start -->
							<div class="breadcrumb-title text-center mb-50">
								<h2 class="big-title"><strong>about</strong> ogaticketr</h2>
							</div>
							<!-- breadcrumb-title - end -->

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- breadcrumb-section - end
	================================================== -->





	<!-- our-management-section - start
	================================================== -->
	<section id="our-management-section" class="our-management-section bg-gray-light sec-ptb-100 clearfix">
		<div class="container">
			<div class="row">

				<!-- section-title - start -->
				<div class="col-lg-4 col-md-12 col-sm-12">
					<div class="section-title text-left mb-50 sr-fade1">
						<small class="sub-title">we are oga at ticketing</small>
						<h2 class="big-title"><strong>No.1</strong> Events Ticketer</h2>
						<a href="{{ route('showSignup') }}" class="custom-btn">
							get started!
						</a>
					</div>
				</div>
				<!-- section-title - end -->

				<div class="col-lg-8 col-md-12 col-sm-12">
					<div class="row">

							<!-- management-item - start -->
							<div class="col-lg-6 col-md-6 col-sm-12">
								<div class="management-item sr-fade2">
									<div class="item-title">
										<h3 class="title-text">
											Who we are
										</h3>
									</div>
									<p class="black-color mb-30">
											OgaTicketr is an event ticketing platform that provides ull ticketing services for various evets such as music festivals, venues, marathons, conferences, hackathons, air guitar contests, political rallies, fundraisers, gaming competitions — you name it, we power it. 
									</p>
								</div>
							</div>
							<!-- management-item - end -->

						<!-- management-item - start -->
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="management-item sr-fade2">
								<div class="item-title">
									<h3 class="title-text">
										our mission
									</h3>
								</div>
								<p class="black-color mb-30">
									To revolutionalize the events industry through superb and easy events management software.
								</p>
							</div>
						</div>
						<!-- management-item - end -->
	

					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- our-management-section - end
	================================================== -->





	<!-- special-offer-section - start
	================================================== -->
	<section id="special-offer-section" class="special-offer-section clearfix" style="background-image: url(assets/images/special-offer-bg.png);">
		<div class="container">
			<div class="row">

				<!-- special-offer-content - start -->
				<div class="col-lg-9">
					<div class="special-offer-content">
						<h2>Looking for something <span>special for your event?</span></h2>
						<p class="m-0">
							Sign up now and we will make your event ticketing unique & unforgettable
						</p>
					</div>
				</div>
				<!-- special-offer-content - end -->

				<!-- event-makeing-btn - start -->
				<div class="col-lg-3">
					<div class="event-makeing-btn">
						<a href="{{ route('showSignup') }}">sign up now</a>
					</div>
				</div>
				<!-- event-makeing-btn - end -->

			</div>
		</div>
	</section>
	<!-- special-offer-section - end
	================================================== -->





    
@stop