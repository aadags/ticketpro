@extends('Public.FrontView.Layouts.Main')

@section('title')
    Disclaimer
@stop

@section('content')

        <div class="main-container col2-left-layout">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="main">

							<div class="row">
								<div class="col-main col-xs-12">
									<div class="padding-s">



<div class="page-title category-title">
        <h1>Disclaimer</h1>
</div>




    
    <div class="category-products">
    
	<br/>
<br/>
<br/>
    <h2>OgaTicketr is an event technology platform that allows anyone to create, share, find and attend new things to do that fuel their passions and enrich their lives. 
		Music festivals, venues, marathons, conferences, hackathons, air guitar contests, political rallies, fundraisers, gaming competitions e.t.c. 
</h2>
<br/>
<br/>
<br/>
<h2>OgaTicketr <b>DOES NOT DIRECTLY SELL THESE TICKETS</b> and will not deal directly with customers relating to sales problems with organisers.</h2>   
<br/>
<br/>
<h2>OgaTicketr <b>DOES NOT DIRECTLY WITHHOLD FUNDS FROM TICKET SALES</b>, Organisers are in full control of their events including ticketing and attendance management.</h2>
<br/>    
<br/> 
<h2>OgaTicketr <b>WILL NOT BE RESPONSIBLE FOR ANY</b> event related issues, Organisers are responsible for their events.</h2>
<br/>    

    </div>


									</div>
								</div>


						</div>
					</div>
				</div>
            </div>
        </div>
   
    
@stop