@extends('Public.FrontView.Layouts.Main')

@section('title')
    Upcoming Events
@stop

@section('headerclass')
header-section default-header-section auto-hide-header clearfix
@stop

@section('bodyclass')
default-header-p
@stop

@section('content')

<!-- event-search-section - start
    ================================================== -->
    <section id="event-search-section" class="event-search-section clearfix" style="background-image: url(assets/images/special-offer-bg.png);">
        <div class="container">
            <div class="row">

                <!-- section-title - start -->
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="section-title">
                        <small class="sub-title">Looking for an event?</small>
                        <h2 class="big-title">event <strong>Search</strong></h2>
                    </div>
                </div>
                <!-- section-title - end -->

                <!-- search-form - start -->
                <div class="col-lg-8 col-md-12 col-sm-12">
                    <div class="search-form form-wrapper">
                        <form action="{{ route('events') }}" method="GET">

                            <ul>
                                <li>
                                    <span class="title">event keyword</span>
                                    <div class="form-item">
                                        <input type="search" name="search" placeholder="Event name, Venue ....." value="{{ $searchString }}">
                                    </div>
                                </li>
                                <li>
                                    <span class="title">event category</span>
                                    <select id="event-category-select" name="category">
                                        <option value="all">All</option>
                                        <option value="concert" {{ ($cat == 'concert')? 'selected' : '' }}>Concert (Party, Festival, Dinner & Award etc.)</option>
                                        <option value="meetup" {{ ($cat == 'meetup')? 'selected' : '' }}>Meetup (Training, Meeting etc)</option>
                                        <option value="sport" {{ ($cat == 'sport')? 'selected' : '' }}>Sport (Fitness, Contest etc)</option>
                                        <option value="conference" {{ ($cat == 'conference')? 'selected' : '' }}>Conference (Seminar, Demo Day etc.)</option>
                                    </select>
                                </li>
                                <li>
                                    <button type="submit" class="submit-btn">search event now</button>
                                </li>
                            </ul>
                            
                        </form>
                    </div>
                </div>
                <!-- search-form - end -->
                
            </div>
        </div>
    </section>
    <!-- event-search-section - end
    ================================================== -->





    <!-- event-section - start
    ================================================== -->
    <section id="event-section" class="event-section bg-gray-light sec-ptb-100 clearfix">
        <div class="container">
            <div class="row justify-content-center">

                <!-- - start -->
                <div class="col-lg-9 col-md-12 col-sm-12">

                    <div class="search-result-form">
                        <form action="#!">
                            <ul>

                                <li>
                                    <span class="result-text">{{ (empty($searchString) && empty($cat))? 'Upcoming events' : 'Search results for "'.$searchString.'"' }}</span>
                                </li>

                            </ul>
                        </form>
                    </div>

                    <div class="tab-content">

                        <div id="grid-style" class="tab-pane fade in active show">
                                    
                                @if(count($events) < 1)
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h3>No upcoming events at the moment.</h3>
                                        </div>
                                    </div>
                                @endif
                                
                                @foreach($events as $key => $event)
                                @if($key == 0 || ($key % 2) == 0 )
					            <div class="row">
					            @endif
                                <!-- event-grid-item - start -->
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="event-grid-item">
                                        <!-- event-image - start -->
                                        <div class="event-image">
                                                <div class="post-date">
                                                        <span class="date">{{ $event->start_date->format('d') }}</span>
                                                        <small class="month">{{ $event->start_date->format('M') }}</small>
                                                    </div>
                                            <img src="{{ $event->images->first()['image_path'] }}" alt="Image_not_found">
                                        </div>
                                        <!-- event-image - end -->

                                        <!-- event-content - start -->
                                        <div class="event-content">
                                            <div class="event-title mb-30">
                                                <h3 class="title">
                                                    {{ $event->title }}
                                                </h3>
                                            </div>
                                            <div class="event-post-meta ul-li-block mb-30">
                                                <ul>
                                                    <li>
                                                        <span class="icon">
                                                            <i class="far fa-clock"></i>
                                                        </span>
                                                        {{ $event->start_date->format('h:i a') }} - {{ $event->end_date->format('h:i a') }}
                                                    </li>
                                                    <li>
                                                        <span class="icon">
                                                            <i class="fas fa-map-marker-alt"></i>
                                                        </span>
                                                        {{ $event->venue_name_full }}
                                                    </li>
                                                </ul>
                                            </div>
                                            <a href="{{ $event->getEventUrlAttribute() }}" class="tickets-details-btn">
                                                tickets & details
                                            </a>
                                        </div>
                                        <!-- event-content - end -->
                                    </div>
                                </div>
                                <!-- event-grid-item - end -->
                               
                                @if(($key+1) % 2 == 0 )
					            </div><!-- /.masonry -->
					            @endif
					            @endforeach
					            @if(count($events) % 2 == 1 )
					            </div><!-- /.masonry -->
					            @endif

                            <div class="row justify-content-center">
                                
                                <!-- pagination - start -->
                                <div class="pagination ul-li clearfix">
                                    <ul>
                                        @if($eventsJson->prev_page_url !== null)
                                        <li class="page-item prev-item">
                                            <a class="page-link" href="{{ $eventsJson->prev_page_url }}">Prev</a>
                                        </li>
                                        @endif
                                        @for($x = 1; $x <= $eventsJson->last_page; $x++)
                                        <li class="page-item {{ ($eventsJson->current_page == $x)? 'active' : '' }}"><a class="page-link" href="?page={{ $x }}">{{ $x }}</a></li>
                                        @endfor
                                        @if($eventsJson->next_page_url !== null)
                                        <li class="page-item next-item">
                                            <a class="page-link" href="{{ $eventsJson->next_page_url }}">Next</a>
                                        </li>
                                        @endif
                                    </ul>
                                </div>
                                <!-- pagination - end -->
                            </div>
                        </div>
                    </div>

                </div>
                <!-- - end -->

            </div>
        </div>
    </section>
    <!-- event-section - end
    ================================================== -->


    
@stop