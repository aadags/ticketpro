@extends('Public.FrontView.Layouts.Main')

@section('title')
    How It Works
@stop

@section('headerclass')
header-section default-header-section auto-hide-header clearfix
@stop

@section('bodyclass')
default-header-p
@stop

@section('content')



<!-- breadcrumb-section - start
	================================================== -->
	<section id="breadcrumb-section" class="breadcrumb-section clearfix">
		<div class="jarallax" style="background-image: url(assets/images/breadcrumb/breadconcert.jpg);">
			<div class="overlay-black">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-6 col-md-12 col-sm-12">

							<!-- breadcrumb-title - start -->
							<div class="breadcrumb-title text-center mb-50">
                                    <p class="big-title">Terms & Conditions</p>
							</div>
							<!-- breadcrumb-title - end -->

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- breadcrumb-section - end
	================================================== -->





	<!-- our-management-section - start
	================================================== -->
	<section id="our-management-section" class="our-management-section bg-gray-light sec-ptb-100 clearfix">
		<div class="container">
			<div class="row">

				<!-- section-title - start -->
				<div class="col-lg-12 col-md-12 col-sm-12">
                        <p>
                                <p>All events managed through this website are subject to the Terms and Conditions outlined below. These Terms and Conditions may change from time to time without prior notice, at our discretion.</p>
                                <p>OgaTicketr may correct errors or inaccuracies and change or update information on this website at any time without notice.
                                <p>All paid tickets listed on the website are subject to a 10% commission on each ticket sale. Free Tickets do not carry any charges.</p>
                        <p>OgaTicketr reserves the right to block you off, suspend your account at any point in time. </p>
                        
                        </p>       
				</div>
				<!-- section-title - end -->
				
			</div>
		</div>
	</section>
	<!-- our-management-section - end
	================================================== -->





	<!-- special-offer-section - start
	================================================== -->
	<section id="special-offer-section" class="special-offer-section clearfix" style="background-image: url(assets/images/special-offer-bg.png);">
		<div class="container">
			<div class="row">

				<!-- special-offer-content - start -->
				<div class="col-lg-9">
					<div class="special-offer-content">
						<p>Looking for something <span>special for your event?</span></p>
						<p class="m-0">
							Sign up now and we will make your event ticketing unique & unforgettable
						</p>
					</div>
				</div>
				<!-- special-offer-content - end -->

				<!-- event-makeing-btn - start -->
				<div class="col-lg-3">
					<div class="event-makeing-btn">
						<a href="{{ route('showSignup') }}">sign up now</a>
					</div>
				</div>
				<!-- event-makeing-btn - end -->

			</div>
		</div>
	</section>
	<!-- special-offer-section - end
	================================================== -->





    
@stop