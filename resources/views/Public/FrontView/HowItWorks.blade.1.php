@extends('Public.FrontView.Layouts.Main')

@section('title')
    How It Works
@stop

@section('content')

        <div class="main-container col2-left-layout">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="main">

							<div class="row">
								<div class="col-main col-xs-12">
									<div class="padding-s">



<div class="page-title category-title">
        <h1>How It Works</h1>
</div>




    
    <div class="category-products">
    <br/>
    <br/>
    <br/>
    <p>
        <h1><b>How To Purchase a Ticket on OgaTicketr.</b></h1><br/><br/>
        <h2>1. Explore the website and browse to the Event of your choice.</h2>
        <h2>2. On the Event page you will find all the details about the event including the available tickets.</h2>
        <h2>3. Choose the number of tickets of your choice you wish to purchase and click on register</h2>
        <h2>4. You will be given a limited time to complete your purchase, 
			during this time your ticket will be booked temporarily in any case their is a high deman for the event tickets.</h2>
        <h2>5. Fill out your details and checkout</h2>
        <h2>6. For paid tickets you will be prompted to fill in your card details after which your ticket will be emailed to you. Ensure to use a valid email for purchase.</h2>
        <h2>7. Ensure to go to the venue with your e-ticket for checking you into your event.</h2>
        <h2>8. If you need fufrther assistance, email us at support@ogaticketr.com or give us a call at (+234) 816 437 5276</h2>
        <br/>   <br/>   

    
</p>
<br/> 

<p>
        <h1><b>For Event Organisers.</b></h1><br/><br/>
        <h2>1. OgaTicketr is a free Event Management and Ticketing service.</h2>
        <h2>2. OgaTicketr charges a 10% commission on paid tickets(only), this includes booking fees payment processing fees. Free Tickets are not billed.</h2>
        <h2>3. You can choose to pass this fee to your customers if you wish.</h2>
		<h2>4. OgaTicketr gives you a wide variety of options which you can use to effectively manage your event:</h2>
		<h4>
		<ul>
        <li>- Fully brandable & customizable - Promote your brand on your event pages</li>
        <li>- Customizable tickets - with QR codes, organiser logos etc.</li>
        <li>- Beautiful mobile friendly event pages</li>
        <li>- Easy attendee management - Refunds, Messaging etc.</li>
        <li>- Data export - attendees list to XLS, CSV etc.</li>
        <li>- Generate print friendly attendee list</li>
        <li>- Ability to manage unlimited events</li>
        <li>- Real-time event statistics</li>
        <li>- Customizable event pages</li>
        <li>- Multiple currency support</li>
        <li>- Quick and easy checkout process</li>
        <li>- Affiliate tracking - track sales volume / number of visits generated etc.</li>
        <li>- Widget support - embed ticket selling widget into existing websites / WordPress blogs</li>
        <li>- Social sharing</li>
        <li>- Messaging - eg. Email all attendees with X ticket</li>
        <li>- Public event listings page for organisers</li>
        <li>- Ability to ask custom questions during checkout</li>
        <li>- Browser based QR code scanner for door management</li>
        </ul>
		</h4>
		<h2>5. To enjoy these features, simply signup and confirm your valid email address to proceed as an organiser.</h2>
		<h2>6. First Timers will be prompted to create an organiser account that will manage your subsequent events</h2>
		<h2>7. Link your bank account to your organiser account at the top left corner side menu under "Account Settings" for auto remittance of paid tickets revenue.</h2>
		<h2>8. Create your event and design your tickets, make your event live and start selling right away.</h2>
		<h2>8. Remittance of funds is done automatically at T + 1 days (T = transaction).</h2>
		<h2>9. Should in any case you experience any difficulties, email us at support@ogaticketr.com or give us a call at (+234) 816 437 5276.</h2>
        <br/>   <br/>   

    
</p>
<br/> 
<br/>   <br/>   
        

    </div>


									</div>
								</div>


						</div>
					</div>
				</div>
            </div>
        </div>
   
    
@stop