    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('assets/images/touch/favicon.ico')  }}">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset("assets/images/ogt_fav.png") }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset("assets/images/ogt_fav.png") }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset("assets/images/ogt_fav.png") }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset("assets/images/ogt_fav.png") }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset("assets/images/ogt_fav.png") }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset("assets/images/ogt_fav.png") }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset("assets/images/ogt_fav.png") }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset("assets/images/ogt_fav.png") }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset("assets/images/ogt_fav.png") }}">
    <meta name="apple-mobile-web-app-title" content="OgaTicket">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset("assets/images/ogt_fav.png") }}">
    <link rel="icon" type="image/png" sizes="160x160" href="{{ asset("assets/images/ogt_fav.png") }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset("assets/images/ogt_fav.png") }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset("assets/images/ogt_fav.png") }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset("assets/images/ogt_fav.png") }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ url("assets/images/ogt_fav.png") }}">
    <meta name="msapplication-config" content="{{ url("assets/images/touch/browserconfig.xml") }}">
    <meta name="application-name" content="OgaTicket">
    <meta name="_token" content="{{ csrf_token() }}" />
    {{--Mobile browser theme colors--}}
    <meta name="theme-color" content="#000000">
    <meta name="msapplication-navbutton-color" content="#000000">
    <meta name="apple-mobile-web-app-status-bar-style" content="#000000">