@extends('Shared.Layouts.Master')

@section('title')
@parent

Event Orders
@stop

@section('top_nav')
@include('ManageEvent.Partials.TopNav')
@stop

@section('menu')
@include('ManageEvent.Partials.Sidebar')
@stop

@section('page_title')
<i class='ico-checkbox-checked mr5'></i>
Authorized Devices to check in tickets for this event.
<span class="page_title_sub_title hide">
 Total Devices: {{ count($attachedDevices) }}
</span>
@stop

@section('head')

@stop

@section('page_header')
<div class="col-md-9 col-sm-6">
        <a data-modal-id="add-device-{{ $event->id }}" data-href="{{route('showAddDevices', ['event_id'=>$event->id])}}" class="btn btn-success loadModal">Authorize New Device</a>
</div>
<div class="col-md-3 col-sm-6">
   
</div>
@stop


@section('content')
<!--Start Attendees table-->
<div class="row">

    @if($attachedDevices->count())

    <div class="col-md-12">

        <!-- START panel -->
        <div class="panel">
            <div class="table-responsive ">
                <table class="table">
                    <thead>
                        <tr>
                            <th>
                               Device Model
                            </th>
                            <th>
                               Device Platform
                            </th>
                            <th>
                               Device App Id
                            </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($attachedDevices as $attachedDevice)
                        <tr>
                            <td>
                                {{ $attachedDevice->device->model }}
                                
                            </td>
                            <td>
                                {{ $attachedDevice->device->platform }}
                            </td>
                            <td>
                                 {{ $attachedDevice->device->oid }}
                            </td>
                            <td>
                                <a href="#" class="btn btn-xs btn-danger"> unauthorize</a>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @else
    @include('ManageEvent.Partials.DevicesBlankSlate')

    @endif
</div>    <!--/End attendees table-->
@stop
