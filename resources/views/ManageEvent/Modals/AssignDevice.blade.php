<div role="dialog"  class="modal fade " style="display: none;">
   {!! Form::open(array('url' => route('postEventDevices', array('event_id' => $event->id)), 'class' => 'ajax')) !!}
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3 class="modal-title">
                    <i class="ico-checkbox-checked"></i>
                    Authorize Check In on New Device</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="form-group">
                            {!! Form::label('app_id', 'Check In App Id', array('class'=>'control-label required')) !!}

                            {!!  Form::text('app_id', Input::old('app_id'),
                                                array(
                                                'class'=>'form-control'
                                                ))  !!}
                        </div>
                    </div>
                </div>
            </div> <!-- /end modal body-->
            <div class="modal-footer">
               {!! Form::button('Cancel', ['class'=>"btn modal-close btn-danger",'data-dismiss'=>'modal']) !!}
               {!! Form::submit('Add Device', ['class'=>"btn btn-success"]) !!}
            </div>
        </div><!-- /end modal content-->
       {!! Form::close() !!}
    </div>
</div>
