@extends('Shared.Layouts.BlankSlate')


@section('blankslate-icon-class')
    ico-checkbox-checked
@stop

@section('blankslate-title')
    No Check In Devices Yet
@stop

@section('blankslate-text')
    New devices will appear here as they are assigned.
@stop

@section('blankslate-body')

@stop

